//TDD - Unit Testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("Hex to RBG conversion", () => {
        it("converts the basic colors", () => {
            const redHex = converter.HextoRBG("ff0000"); // 255,0,0
            const greenHex = converter.HextoRBG("00ff00"); // 0,255,0
            const blueHex = converter.HextoRBG("0000ff"); // 0,0,255

            expect(redHex[0]).to.equal(255);
            expect(redHex[1]).to.equal(0);
            expect(redHex[2]).to.equal(0);
            expect(greenHex[0]).to.equal(0);
            expect(greenHex[1]).to.equal(255);
            expect(greenHex[2]).to.equal(0);
            expect(blueHex[0]).to.equal(0);  
            expect(blueHex[1]).to.equal(0);  
            expect(blueHex[2]).to.equal(255);  
        });
    });
});
