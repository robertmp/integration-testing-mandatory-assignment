

module.exports = {
    /**
     * Converts Hex to RBG
     */
    HextoRBG: (hex) => {
        var values = hex.split('');
        const red = parseInt(values[0].toString() + values[1].toString(), 16);
        const green = parseInt(values[2].toString() + values[3].toString(), 16);
        const blue = parseInt(values[4].toString() + values[5].toString(), 16);
        return [red,green,blue];
    }
}
